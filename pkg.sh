#!/bin/sh
set -eu
file_name=ncgdmw-lua.zip

cleanup() {
    for f in CHANGELOG.md FAQ.md LICENSE README.md; do
        mv "00 Core/docs/$f" .
    done
}

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

for f in CHANGELOG.md FAQ.md LICENSE README.md; do
    mv $f "00 Core/docs/"
done

zip --must-match \
    --recurse-paths\
    --verbose \
    $file_name \
    "00 Core" \
    "01 ClassicNCGD" \
    "02 NewNCGD" \
    "03 NewNCGDDoubleSkillBonuses" \
    '04 AltStart (For Total Conversions)' \
    "05 Starwind" \
    "06 Patches" \
    version.txt \
    --exclude ./00\ Core/ncgdmw-vanilla-birthsigns-patch.yaml \
    --exclude ./01\ ClassicNCGD/\*.yaml \
    --exclude ./02\ NewNCGD/\*.yaml \
    --exclude ./03\ NewNCGDDoubleSkillBonuses/\*.yaml \
    --exclude ./04\ AltStart\ \(For\ Total\ Conversions\)/\*.yaml \
    --exclude ./04\ AltStart\ \(For\ Total\ Conversions\)/scripts\* \
    --exclude ./05\ Starwind/\*.yaml \
    --exclude ./05\ Starwind/scripts\* \
    --exclude ./06\ Patches/\*.d\* \
    || cleanup
sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt

cleanup
