======================================================
Natural Character Growth and Decay - Morrowind Edition 
======================================================

Created by Greywander May 2017

* Also Try These Mods *
=======================

Better Regen - Slowly regenerate health and magicka, if your Endurance/Willpower is over 50.  Not OP like some
               regen mods.
http://www.nexusmods.com/morrowind/mods/44969/?

CarryOn - Doubles your carry weight and increases your running speed, but the effects of fatigue and encumbrance
          are more potent.
http://www.nexusmods.com/morrowind/mods/44972/?

Magicka Based Skill Progression - ncgdMW Compatibility Version
        - Makes magical skill progression based on the amount of magicka used, instead of number of spells cast.
          Original by HotFusion4, edited by me to work with ncgdMW.
http://www.nexusmods.com/morrowind/mods/44973/?

* Introducing NCGDMW Lua Edition 1.0 *
======================

This is the initial release of the OpenMW-Lua rewrite of NCGDMW!

The mod has been ported from its MWScript roots to make use of the OpenMW-Lua API and all its features, including:

* An inteface for other mods to hook into NCGDMW functionality

As of this release, not all features from the classic version of NCGDMW are implemented. Planned features include:

* Post-chargen new game config popup menu
* Play a sound when a stat decays
* Uncapped attributes and skills
* Configurable caps for all/individual attributes/skills
* Optional level up song and animation
* View decay progress by mousing over a skill
* Show level progress in the stats menu

From this release forward, the Lua Edition release notes can be found in the `CHANGELOG.md` file.

* New in Version 3.4 *
======================

This is the final release of NCGDMW, it is not compatible with OpenMW 0.48.0 and up.

Applied torgue's fix for werewolves (seen here: https://forums.nexusmods.com/index.php?showtopic=5660147%2F#entry79474898).

* New in Version 3.3 *
======================

Attribute calculations have been changed in order to account for OpenMW storing
attributes and skills as floats instead of ints.

This would previously cause the "unexpected value" dialogue to display when it
shouldn't have.

Many thanks to Atahualpa for implementing the fix for this!

* New in Version 3.2 *
======================

When stat values are displayed to the player, they no longer overflow with huge
numbers (it now uses `%G` instead of `%.0g`).

* New in Version 3.1 *
======================

This small update brings a few new things.  First of all I just want to remind everyone that there are now two
different versions: the regular version and the "alt start" version.  Only activate one of these, depending on if
you are using an Alternate Start mod (or TES3MP) or not.  If you're using the Alt Start version, just drink the
potion to start ncgdMW whenever you're ready.

The major new thing in 3.1 is that you can now install ncgdMW on an existing save file, including allowing you to
update from an older version.  While the mod is initializing, choose the "existing save" option and you'll be
prompted to input your initial attributes after character creation.  If using vanilla races and birthsigns, you
can look those up on the wiki, or consult the readme for any race or birthsign mod you are using.  Don't forget to
account for your class's favored attributes!

Due to scripting limitations, or perhaps my own lack of experience, I had to use a rather circuitous method to allow
the player to input their initial attributes.  For each attribute, you'll need to select the value of the TENS place
first, then the value of the ONES place.  For example, if your initial Strength was 47, you would first choose "4x"
then "x7", and these combine to make 47.  If you've had permanent modifications to your attributes, such as from
using the Bittercup, you should figure those in, too, but double their effect (as attributes are cut in half during
chargen).

* New in Version 3.0 *
======================

I've had a previously unreleased "version 2.2" that included some improvements to the decay system.  I've since
forgotten the details of the improvements, but they are included in 3.0.

A number of compatibility fixes have been made.  Previously, while initializing NGCD would subtract 25 from your
vanilla attributes.  This wasn't a problem for vanilla races, none of which start with less than 30 in any
attribute, but custom races could start with less than 0 in attributes.  Now NGCD just cuts your vanilla
attributes in half, so it should be compatible with custom races.  As a side effect of this, attributes will
generally start a bit higher than they used to, and will cluster closer to an average than they used to (which is
to say that very high or very low vanilla values won't have as much of an effect on your recalculated attribute
values).

The second compatibility fix has to do with effects that modify your attributes.  One such effect is the Bittercup,
an item that raises your highest attribute while lowering the lowest, permanently.  Previously, NCGD would show an
error informing you that it had detected the unexpected value, and then automatically revert to the expected value,
essentially overriding the effect.  NCGD now gives you the option to either keep the unexpected value (making the
effect stick) or revert to the expected value (as it previously did).

I've also fixed a bug that would prevent skills from decaying below 150, and would have stopped a skill from
decaying at all if you raised it to 200.  This had to do with not allowing a skill to decay below half of the
highest rank you've raised it to, and not taking the mastery fortify effects into consideration.  So a skill rank
of 150 was looking like 75 when it made the check, which is half of 150.  Skill mastery is now properly accounted
for.

- For those using Alternate Start mods or TES3MP -

There is now a modified version of this mod available for download.  Rather than starting automatically, this mod
places a potion in your inventory.  Drink the potion when you are ready for the mod to start.  This still needs
testing, so please let me know if you still have issues with this mod.

- Known Issues -

When a skill at 100 or any increment of 25 above that (125, 150, 175, etc) decays, it will briefly appear to be 25
points higher than it really is before correcting itself.  I'm not sure exactly what's causing this, but it appears
that a skill mastery effect is being applied when it shouldn't be, raising the skill by an extra 25 points.  Since
the issue seems to fix itself, I've decided not to worry about it.  Let me know if this causes you any serious
bugs.


* New in Version 2.0 *
======================

The decay system has been reworked to incorporate your Intelligence and Level.  As your Level increases, your
skills will decay faster.  As your Intelligence increases, it will slow down the rate at which skills decay.
I've also added information to the tooltips for both skills and attributes, the attributes moreso just to inform
you of some of the changes (e.g. Willpower gives health, Intelligence slows decay), while the skills now tell you
which attributes they influence.  They're always listed in order starting with the primary attribute.  Also new is
that you will now be notified when attributes increase, and when skills and attributes decay.

As always, I'm looking for feedback regarding this mod.  I plan on using this in my own playthrough, but the more
playtesters the better we'll be able to check for bugs or balance issues.  Enjoy!

* New in Version 1.1 *
======================

I don't know why I didn't do this earlier, but I looked at the skills in Oblivion and how they relate to attributes
and some of their connections made more sense than mine.  For example, Acrobatics, as a mobility skill, makes more
sense to have Speed as a primary attribute than Short Blade or Hand To Hand does.  All I've done is update which
skills influence which attributes and by how much.

0. Introduction
===============

We've all said it before: the leveling system in vanilla Morrowind sucks.  Having to micromanage your skill gains
in order to make sure you get those juicy x5 multipliers at a level up, only to end up with straight 100's across
all your attributes (with the possible exception of Luck) when you're just at a moderate level.

And so there were a number of leveling mods that sought to alleviate this burden and make playing the game more fun.
Mods like Linora's Leveling Mod keep things straightforward and retain the flavor of the original, simply reducing
the micromanagement needed to get a good multiplier.  Mods like Galsiah's Character Development instead chose to
take a radical departure from the original and instead try something new, a "Natural Grow" system that distributes
points to your attributes automatically based on your playstyle.

It is this second vein that I've chosen to take for this mod.  GCD had its own issues, notably that it didn't seem
to allow attributes to grow past 95 when playing on OpenMW, when it had initially been designed to allow the player
to break the caps on skills and attributes.  At first, I lamented that a mod I felt like I couldn't live without
didn't work quite as it should on OpenMW.  Then, an amazing discovery about how OpenMW handles Get/Mod/SetStat
script commands lead me to develop my own interpretation of a Natural Grow leveling mod.

This mod is not to be confused with a Skyrim mod by the same name.  That was a mod I created a while ago and, due
to differences in the gameplay and engine, had a different albeit related set of goals and requirements.

1. Requirements
===============

* This mod REQUIRES OpenMW *

This mod not only breaks the limits of the original Morrowind scripting engine with an impressive 5000+ lines of
code in the main script, it also relies on OpenMW's implementation of the SetAttribute and SetSkill scripting
commands, which differs from that of the original Morrowind.

This mod will never and can never be compatible with the original Morrowind.

As of version 3.1 it's now possible to install ncgdMW on an existing save file.  Follow the on-screen instructions
to input your starting attributes as they were immediately after chargen.  You can look up your race and birthsign
info on the wiki if you need to, and don't forget to figure in your class's favored attributes!

Removing ncgdMW from an existing save may be difficult, and will likely have permanent effects on your attributes.
If you're updating from an old version, make sure to clean your save and then install the new version as if you
where installing it on an existing save.

2. Installation
===============

1. Download ncgdMW.7z and place it wherever you keep your mods.
2. Unzip it to it's own folder.  If it unzips into your main mod directory, create a folder and place it inside.
3. Find openmw.cfg and add a line that says 'data="[path to your mod directory]/ncgdMW"'

You should be able to find the config file in one of these locations:
    Linux: $HOME/.config/openmw
    Windows: C:\Users\Username\Documents\my games\openmw (username will vary, as may drive and language)
    Mac: /Users/Username/Library/Preferences/openmw

If you're having trouble understanding any of that, you can just unzip the file and drop ncgdMW.omwaddon into your
data directory.  There are no other assets other than the .omwaddon, so you don't need to worry too much about
keeping it separated from other mods.  You can also check here for more information on how to install mods in
OpenMW:

https://wiki.openmw.org/index.php?title=Mod_installation

If you're using an Alternate Start mod or TES3MP, then you may run into some bugs.  If you're having issues, try
using ncgdMW_alt_start.omwaddon INSTEAD OF ncgdMW.omwaddon.  Drink the potion that appears in your inventory to
start the mod when it is safe.

3. Uninstallation
=================

Open openmw.cfg and comment out the line pointing to the ncgdMW directory.  Just put a '#' in front and it will
disable that entire line.  Or you can just disable the .omwaddon in the launcher.

Continuing a playthrough after you've removed the mod will most likely have unexpected and permanent effects.

4. Updating
===========

Make sure to clean your save to insure that the old version of ncgdMW has been removed.  Then install the new
version and choose "existing save" during the initialization prompt.  Follow the instructions to input your initial
attributes as they were immediately after chargen BEFORE ncgdMW started messing with them.  You can look up the
starting values for races and birthsigns on the wiki, or if you are using custom races or birthsigns you can consult
the readme for that mod.  Don't forget to account for your class's favored attributes.

5. Compatiblity
===============

As previously stated, this mod requires OpenMW, and is not compatible with the original Morrowind engine.

This mod should, in theory, be compatible with all other mods except for those that alter the leveling system or
how the player gains Health.  Galsiah's Character Development is of course incompatible.  A mod such as Talrivian's
State Based HP would conflict with ncgdMW's own system for determining Health.

Another mod that seems less obvious is Magicka Based Skill Progression, which uses a (limited) number of skill
books to increase magic skills in a way that the game recognizes for level up purposes (which is important for
vanilla leveling, but not for ncgdMW).  It should work, but as ncgdMW allows skills to increase beyond 100 you
would eventually run out of skill books and be unable to raise your magic skills.  There is a workaround that
involves cleaning your save, so that might be an option.  There's a link to an edited version of this mod that is
compatible with ncgdMW at the top of this readme.

6. Features
===========

 * Your attributes grow automatically as your skills increase.
 * Your skills and attributes can grow past 100, but...
 * It becomes increasingly difficult to increase your skills the farther past 100 you go.
 * Your skills will also decay over time.  (Optional)
 * Finally, since vanilla levelups are disable, I've implemented my own system of determining the player's Health.

~ Attribute Growth ~
--------------------

Each skill contributes points to three attributes, plus Luck.  Skills have a primary attribute (4 points), a
secondary attribute (2 points), and a tertiary attribute (1 point), with Luck acting as a second tertiary
attribute.  I've changed some the primary attribute of some skills, sometimes for balance, sometimes for realism
(Long Blade is now an Agility skill with Strength as a secondary, while Marksman is now a Strength skill with
Agility as a secondary).

See the included spreadsheet for how skills are related to attributes, or see the ASCII chart further down.

The amount of points a skill contributes to attributes is proportional to the skill squared.  This means a skill
of 10 will contribute 4 times as many points as a skill of 5 (5 * 5 = 25, 10 * 10 = 100).  Generally, you'll grow
faster by increasing your skills that are highest, while increasing lower skills will give less of a benefit.  This
doesn't mean you have to specialize, quite the contrary.  You'll get higher attributes overall by raising, say,
Axe and Destruction (which don't share any attributes) than you will by raising Axe and Spear (which are linked to
the same attributes).  Conversely, Raising your Axe and Spear together will result in a higher Strength score than
raising a multitude of Strength skills to minor levels.

~ Skill Growth ~
----------------

When a skill reaches 100, it is reset back to 75 but you are granted a +25 Fortify Skill bonus.  (This will show up
in your current magic effects as 'Skill Mastery'.)  If your skill drops below 75, it will be raised back up and you
will lose the bonus.  You shouldn't even notice the difference.  When a skill increases, the vanilla message
showing the new value of your base skill (1-100) will appear, along with an identical scripted message showing your
'real' skill (base skill + mastery bonus).

When you have 'Skill Mastery', that skill becomes much harder to raise, requiring you to increase the skill twice
before it will actually increase.  Once your skill reaches 125 (with the bonus), you'll need three skillups for
each one, and every 25 skill ranks beyond will require one additional skillup to each the next skill rank.  A
message will appear notifying you that your skill increase isn't enough and how many more you need.

There is a new skill cap of 475.  I could have made it higher, but I didn't see much of a point.  Good luck
reaching the cap.

~ Skill Decay ~
---------------

Skills can optionally decay, with three possible speeds.  Intelligence is now important for slowing down the decay
of skills.  As your level rises, your skills will decay faster, but Intelligence can slow it down somewhat.  Skills
decay independently from one another, with the exact time of decay being randomized within limits determined by
your level and your Intelligence.  When a skill decays, you lose an entire skill rank (I couldn't see a way to only
remove skill progress, so this is what we have).

Fortunately, there are some things that soften the effects of decay.  The first, of course, being the relatively
slow speed of decay, even at the fasted speed.  Second is that skills will not decay below certain values.  A skill
will never decay below 15, ever.  ncgdMW also remembers the highest rank you've ever reached with a skill, and will
not let a skill decay below half your highest rank.  Third, when a skill is below its highest rank, you get double
skill increases until you've caught back up.  Finally, regardless of your chosen decay speed, your first skill
decays will happen between 15 and 30 days* after you've started play, giving you plenty of time to figure out your
character build.

*As your level rises, skills will decay faster, which can cause skills to decay before 15 days if you raise your
level high enough.

~ Starting Out ~
----------------

Speaking of, your choices at character creation will have a much larger impact on your starting abilities, with
attributes theoretically starting as low as 12 or higher than 80 (on standard growth).  If you pick a race/class
combination that complement one another, you might even start at a level higher than 1 (a breton mage starts at
level 4).  That said, if you don't like your initial choices, you can just start training different skills.  Your
choices of Major and Minor skills, as well as your specialization, have no effect except for the starting leve of
your skills.  Your favored attributes do stick, however, so choose those carefully.

~ Health Formula ~
------------------

Lastly, because the level menu is disable I've had to implement my own system for determining the player's Health.
I've decided to divorce Health gains entirely from level ups (which are used solely for leveled lists now), instead
simply setting the player's Health to Endurance + half your Strength + a quarter of your Willpower.  The inclusion
of Willpower here is partly to make Willpower appealing to physical characters, and partly to help mages be not so
squishy.  If you have all three at 100 then you'll have a modest 175 Health (100 + 50 + 25).  Remember that you can
increase attributes beyond 100, though.

7. Skill Chart
==============

A '4' indicates a primary attribute, a '2' indicates a secondary attribute, and a 1 indicates a tertiary attribute.
Each skill distributes a total of 8 points, and each attribute receives a total of 27 points.

See also the included spreadsheet if you want to play around with the values yourself.

===========|========|=======|=========|=====|============|=========|===========|====|
           |Strength|Agility|Endurance|Speed|Intelligence|Willpower|Personality|Luck|
===========|========|=======|=========|=====|============|=========|===========|====|
LongBlade  |    2   |   4   |         |  1  |            |         |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
BluntWeapon|    4   |       |    1    |     |            |    2    |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Axe        |    4   |   2   |         |     |            |    1    |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Armorer    |    1   |       |    4    |     |            |         |     2     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
MediumArmor|        |       |    4    |  2  |            |    1    |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
HeavyArmor |    1   |       |    4    |  2  |            |         |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Spear      |    4   |       |    2    |  1  |            |         |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Block      |    2   |   1   |    4    |     |            |         |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Athletics  |        |       |    2    |  4  |            |    1    |           |  1 |
===========|========|=======|=========|=====|============|=========|===========|====|
           |Strength|Agility|Endurance|Speed|Intelligence|Willpower|Personality|Luck|
===========|========|=======|=========|=====|============|=========|===========|====|
Alchemy    |        |       |    1    |     |      4     |         |     2     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Enchant    |        |       |         |     |      4     |    2    |     1     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Conjuration|        |       |         |     |      4     |    1    |     2     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Alteration |        |       |         |  1  |      2     |    4    |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Destruction|        |       |         |     |      2     |    4    |     1     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Mysticism  |        |       |         |     |      4     |    2    |     1     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Restoration|        |       |    1    |     |            |    4    |     2     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Illusion   |        |   1   |         |     |      2     |         |     4     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Unarmored  |        |       |    2    |  4  |            |    1    |           |  1 |
===========|========|=======|=========|=====|============|=========|===========|====|
           |Strength|Agility|Endurance|Speed|Intelligence|Willpower|Personality|Luck|
===========|========|=======|=========|=====|============|=========|===========|====|
Acrobatics |    1   |   2   |         |  4  |            |         |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Security   |        |   4   |         |     |      2     |         |     1     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Sneak      |        |   4   |         |  1  |            |         |     2     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
LightArmor |        |   1   |    2    |  4  |            |         |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Marksman   |    4   |   2   |         |  1  |            |         |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
ShortBlade |        |   4   |         |  2  |            |         |     1     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
HandToHand |    4   |   2   |    1    |     |            |         |           |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Mercantile |        |       |         |     |      2     |    1    |     4     |  1 |
-----------|--------|-------|---------|-----|------------|---------|-----------|----|
Speechcraft|        |       |         |     |      1     |    2    |     4     |  1 |
===========|========|=======|=========|=====|============|=========|===========|====|
           |Strength|Agility|Endurance|Speed|Intelligence|Willpower|Personality|Luck|
===========|========|=======|=========|=====|============|=========|===========|====|

8. Legal
========

Feel free to modify and distribute this file, just remember to give me credit.
